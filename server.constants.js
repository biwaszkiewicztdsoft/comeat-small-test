const development = process.env.NODE_ENV !== 'production';

if(development){
  global.HOST_URL = "http://localhost:3000/";
  global.PORT = '3000';
} else {
  global.HOST_URL = "";
  global.PORT = '';
}




