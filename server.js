// Get dependencies
const express = require('express');
const path = require('path');
const https = require('https');
const http = require('http');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const MongoInit = require('./server/mongo.init');
const fs = require('fs');
require('./server.constants');

// Get our API routes
const FeedbackAPI = require('./server/routes/feedback');
const UsersAPI = require('./server/routes/users');

let app = express();
mongoose.Promise = global.Promise;

const development = process.env.NODE_ENV !== 'production';

if (development) {
  mongoose.connect('localhost:27017/comeattest');
} else {
  mongoose.connect('');
}


// Parsers for POST data
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit: 50000}));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, PUT, GET, PATCH, DELETE, OPTIONS');
  next();
});

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));


// Set our api routes
app.use('/feedback', FeedbackAPI);
app.use('/users', UsersAPI);


// Catch all other routes and return the index file (angular)
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */

app.set('port', global.PORT);

/**
 * Create HTTPS server if live.
 * Create HTTP if local
 */
let server = {};
if (development) {
  server = http.createServer(app);
} else {
  server = https.createServer(credentials, app);
}


/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(global.PORT, () => {

  try {
    const mongoInit = new MongoInit();
    mongoInit.loadMayankIfNotExist();
  } catch (e) {
    console.log(e);
  }

  console.log("HOST_URL: " + global.HOST_URL);
});

