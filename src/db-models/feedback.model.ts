export class FeedbackModel {
  constructor(public overallScore: number,
              public propertyScore: number[],
              public userExperience: string) {
  }
}
