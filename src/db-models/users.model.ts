import {PostData} from '../app/home/home-post/postData.model';
export class UsersModel {
  constructor(public name: string,
              public overallScore: number,
              public propertyScore: number[],
              public feedback: PostData[] = []) {
  }
}
