import {NgModule, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormComponent} from './form.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {FormService} from './form.service';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    SharedModule
  ],
  providers: [
    FormService
  ],
  declarations: [FormComponent],
  exports: [FormComponent]
})
export class FormModule {
}
