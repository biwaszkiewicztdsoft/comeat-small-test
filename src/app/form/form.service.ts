import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FeedbackModel} from '../../db-models/feedback.model';
import {Globals} from '../app.globals';

@Injectable()
export class FormService {

  headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {
  }

  postFeedback(feedback: FeedbackModel) {
    const body = JSON.stringify(feedback);
    return this.http.post(Globals.HOST_URL + 'feedback', body, {headers: this.headers});
  }

}
