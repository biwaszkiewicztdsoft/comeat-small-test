export class FormSlider {
  constructor(public name: string,
              public value: number = 50) {
  }
}
