import {Component, OnInit} from '@angular/core';
import {FormSlider} from './form-slider.model';
import {FormService} from './form.service';
import {FeedbackModel} from '../../db-models/feedback.model';
import {Globals} from '../app.globals';
import {routerTransition} from '../app-router.animations';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  animations: [
    routerTransition()
  ],
  host: {'[@routerTransition]': ''}
})
export class FormComponent implements OnInit {

  sliders: FormSlider[] = [];

  overall: number = 50;

  userExperience: string = '';


  constructor(private fs: FormService) {
  }

  ngOnInit() {
    for (const type of Globals.PROPERTY_TYPES) {
      this.sliders.push(
        new FormSlider(
          type
        )
      );
    }
  }


  updateOverall() {
    const mapValue = this.sliders.map(a => a.value);
    const sum = mapValue.reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    });

    this.overall = +(sum / this.sliders.length).toFixed(0);
  }

  submitClick() {

    if (this.userExperience === '') {
      alert('User experience cannot be empty');
      return;
    }

    const propertyScore = this.sliders.map(a => a.value);

    const feedback = new FeedbackModel(
      this.overall,
      propertyScore,
      this.userExperience
    );
    this.fs.postFeedback(feedback).subscribe(
      (res) => {
        console.log(res);
      }
    );
  }


}
