import {Component} from '@angular/core';
import {Globals} from './app.globals';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {
    if (environment.production) {
      Globals.HOST_URL = '';
    } else {
      Globals.HOST_URL = 'http://localhost:3000/';
    }
  }

}
