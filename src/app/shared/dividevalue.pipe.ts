import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dividevalue'
})
export class DividevaluePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) {
      return value;
    }

    const toReturn = value / 10;
    return toReturn.toFixed(1);
  }

}
