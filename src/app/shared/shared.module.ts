import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DividevaluePipe } from './dividevalue.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DividevaluePipe],
  exports:[DividevaluePipe]
})
export class SharedModule { }
