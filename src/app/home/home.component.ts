import {Component, OnInit} from '@angular/core';
import {FormSlider} from '../form/form-slider.model';
import {UsersModel} from '../../db-models/users.model';
import {HomeService} from './home.service';
import {PostData} from './home-post/postData.model';
import {Globals} from '../app.globals';
import {routerTransition} from '../app-router.animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    routerTransition()
  ],
  host: { '[@routerTransition]': '' }
})
export class HomeComponent implements OnInit {


  user: UsersModel;
  sliders: FormSlider[];

  constructor(private hs: HomeService) {
  }

  ngOnInit() {
    this.hs.getUser().subscribe(
      (res: UsersModel) => {
        this.user = new UsersModel(
          res.name,
          res.overallScore,
          res.propertyScore,
        );

        this.sliders = [];

        for (let i = 0; i < Globals.PROPERTY_TYPES.length; i++) {
          this.sliders.push(
            new FormSlider(
              Globals.PROPERTY_TYPES[i],
              this.user.propertyScore[i]
            )
          );
        }

        for (const post of res.feedback) {
          this.user.feedback.push(
            new PostData(
              post.overallScore,
              post.userExperience
            )
          );
        }
        console.log(this.user);
      }
    );
  }

}
