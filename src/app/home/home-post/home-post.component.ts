import {Component, Input, OnInit} from '@angular/core';
import {PostData} from './postData.model';

@Component({
  selector: 'app-home-post',
  templateUrl: './home-post.component.html',
  styleUrls: ['./home-post.component.scss']
})
export class HomePostComponent implements OnInit {
  @Input() postData: PostData;

  constructor() {
  }

  ngOnInit() {
  }

}
