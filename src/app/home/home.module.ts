import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../shared/shared.module';
import {HomePostComponent} from './home-post/home-post.component';
import {HomeService} from './home.service';

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    CommonModule
  ],
  providers: [
    HomeService
  ],
  declarations: [HomeComponent, HomePostComponent],
  exports: [HomeComponent]
})
export class HomeModule {
}
