import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../app.globals';

@Injectable()
export class HomeService {

  headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) {
  }

  getUser() {
    return this.http.get(Globals.HOST_URL + 'users');
  }
}
