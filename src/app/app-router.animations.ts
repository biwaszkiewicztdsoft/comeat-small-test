import {trigger, animate, style, transition} from '@angular/animations';

export function routerTransition() {
  return trigger('routerTransition', [
    transition('void => *', [
      style({opacity:0}),
      animate('150ms 150ms', style({opacity:1}))
    ]),
    transition('* => void', [
      style({opacity:1}),
      animate('150ms', style({opacity:0}))
    ])
  ]);
}
