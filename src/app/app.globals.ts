export class Globals {
  public static HOST_URL: string = '';
  public static readonly PROPERTY_TYPES: string[] = [
    'Online Communication',
    'Sociablity',
    'Atmosphere',
    'Food',
    'Value for money'
  ];
}
