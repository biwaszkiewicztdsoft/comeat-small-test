const express = require('express');
const router = express.Router();
const Users = require('../models/users.model');
const Feedback = require('../models/feedback.model');
const _ = require('lodash');

router.post('/', (req, res) => {

  Users.findOne({name: 'Mayank'},
    (err, user) => {
      if (err) {
        return res.status(500).json({
          title: 'Error',
          error: err
        });
      }

      const feedback = new Feedback(
        {
          overallScore: req.body.overallScore,
          propertyScore: req.body.propertyScore,
          userExperience: req.body.userExperience
        }
      );

      feedback.save(
        (err, savedFeedback) => {
          if (err) {
            return res.status(500).json({
              title: 'Error',
              error: err
            });
          }

          //add new feedback
          user.feedback.push(savedFeedback._id);


          user.save(
            (err, udpatedUser) => {
              if (err) {
                return res.status(500).json({
                  title: 'Error',
                  error: err
                });
              }


              return res.status(201).json({
                title: 'Feedback stored',
                obj: udpatedUser
              });
            }
          )

        }
      )

    });

});


module.exports = router;
