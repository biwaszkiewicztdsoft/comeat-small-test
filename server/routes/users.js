const express = require('express');
const router = express.Router();
const Users = require('../models/users.model');


router.get('/', (req, res) => {
  Users.findOne({name: 'Mayank'})
    .populate('feedback')
    .exec(
      (err, user) => {
        if (err) {
          return res.status(500).json({
            title: 'Error',
            error: err
          });
        }


        if (user.feedback.length !== 0) {

          //collect all feedbacks propertyScore
          const allFeedbackValues = [];

          for (let feedback of user.feedback) {
            allFeedbackValues.push(
              feedback.propertyScore
            );
          }

          //sum all feedbacks propertyScore
          let sumPropertyScore = new Array(allFeedbackValues[0].length).fill(0);

          for (let i = 0; i < allFeedbackValues.length; i++) {
            for (let j = 0; j < sumPropertyScore.length; j++) {
              sumPropertyScore[j] += allFeedbackValues[i][j];
            }
          }

          //make avg
          sumPropertyScore = sumPropertyScore.map(
            a => {
              return ((a / allFeedbackValues.length).toFixed(0)) / 1;
            }
          );

          user.propertyScore = sumPropertyScore;

          //update overallScore
          const reducedPS = sumPropertyScore.reduce(
            (a, c) => {
              return a + c;
            }
          );

          user.overallScore = ((reducedPS / sumPropertyScore.length).toFixed(0)) / 1;


          user.save(
            (err, updatedUser) => {
              if (err) {
                return res.status(500).json({
                  title: 'Error',
                  error: err
                });
              }

              res.json(
                updatedUser
              );
            }
          );


        } else {
          res.json(
            user
          );
        }

      }
    )
});


module.exports = router;
