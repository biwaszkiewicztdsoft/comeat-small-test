class MongoInit {

  constructor() {
    this.Users = require('./models/users.model');
  }

  loadMayankIfNotExist() {
    this.Users.findOne({name: 'Mayank'},
      (err, user) => {
        if (err) {
          throw new Error(err);
        }

        if (!user) {
          const mayank = new this.Users(
            {
              name: 'Mayank',
              overallScore: 0,
              propertyScore: [0, 0, 0, 0, 0],
              feedback: []
            }
          );

          mayank.save(
            (err) => {
              if (err) {
                throw  new Error(err);
              }
              console.log('User Mayank has been created');
            }
          )
        } else {
          console.log('Mayank exist');
        }

      });
  }


}

module.exports = MongoInit;
