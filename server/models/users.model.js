const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let schema = new Schema({
  name: {type: String, required: true},
  overallScore: {type: Number, required: true},
  propertyScore: [{type: Number}],
  feedback: [{type: Schema.Types.ObjectId, ref: 'Feedback'}]
}, {collection: 'Users'});

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('Users', schema);
