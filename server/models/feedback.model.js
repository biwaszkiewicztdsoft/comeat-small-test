const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let schema = new Schema({
  overallScore: {type: Number, required: true},
  propertyScore: [{type: Number}],
  userExperience: {type: String, required: true}
}, {collection: 'Feedback'});

schema.plugin(mongooseUniqueValidator);
module.exports = mongoose.model('Feedback', schema);
